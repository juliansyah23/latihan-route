import React from "react";
import Home from "./view/Home";
import "./assets/style.css";
import { Route, Switch } from "react-router-dom";
import About from "./view/About";
import Contact from "./view/Contact";
import Detail from "./view/Detail";
import NotFound from "./view/NotFound";

const App = () => {
  return (
    <div>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/about" exact component={About} />
        <Route path="/contact" exact component={Contact} />
        <Route path="/detail/:id" exact component={Detail} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default App;
