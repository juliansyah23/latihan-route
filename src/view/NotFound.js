import React, { Component } from "react";
import Main from "../layout/Main";

class NotFound extends Component {
  render() {
    return (
      <Main>
        <h1>
          <center>404 Not Found</center>
        </h1>
      </Main>
    );
  }
}

export default NotFound;
