import React, { Component } from "react";
import Main from "../layout/Main";

const Contact = () => {
  return (
    <Main>
      <h1>Contact</h1>
      <div>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur voluptatum necessitatibus magni laborum sunt reprehenderit nemo illum numquam at ea, unde corrupti molestias provident, tempore beatae culpa eum molestiae
          incidunt! Nihil qui sequi consectetur tenetur odit dicta ullam quod obcaecati ex maxime et, minima animi, cumque nobis sint similique odio!
        </p>
      </div>
    </Main>
  );
};

export default Contact;
