import React, { Component } from "react";
import { Link } from "react-router-dom";
import Main from "../layout/Main";

class Home extends Component {
  state = {
    games: [
      {
        id: 1,
        title: "Ingin Bermain CS:GO Di Map VALORANT? Ini Dia Solusinya!",
        thumb: "https://thelazy.media/wp-content/uploads/2021/04/valo-cs-218x150.jpg",
        author: "Joshua Hadinata",
        tag: "esports",
        time: "April 12, 2021",
        desc: "VALORANT saat ini bisa dikatakan menjadi sebuah game FPS yang sukses dipasaran. Dengan perpaduan mechanics yang mudah dipahami oleh pemain game FPS dan juga sejajaran skill set yang membuat game ini...",
        key: "2021/04/12/ingin-bermain-csgo-di-map-valorant-ini-dia-solusinya",
      },
      {
        id: 2,
        title: "PUBG Mobile Kini Rilis Map Yang Mereka Janjikan Yaitu Karakin",
        thumb: "https://thelazy.media/wp-content/uploads/2021/04/pubg-karakin-map-hut-tunnels-compounds-loot-drop-the-loadout-1080-1920-218x150.jpg",
        author: "Joshua Hadinata",
        tag: "esports",
        time: "April 8, 2021",
        desc:
          "PUBG Mobile yang sebelumnya memang memberikan kabar akan bawakan map Karakin ke dalam game nya, kini mereka wujudkan bersamaan dengan beberapa in-game mechanic lainnya.\n\n\n\nPUBG Mobile yang selama ini menghasilkan keuntungan luar...",
        key: "2021/04/08/karakin-kini-tersedia-di-pubg-mobile",
      },
      {
        id: 3,
        title: "Na’Vi Kini Resmi Datangi RAMZES Untuk Roster DOTA 2 Mereka",
        thumb: "https://thelazy.media/wp-content/uploads/2021/04/17101-Untitled-design-53-1-218x150.jpg",
        author: "Joshua Hadinata",
        tag: "esports",
        time: "April 5, 2021",
        desc: "Membuat perubahan drastis dalam sebuah roster tim yang bisa dibilang bermain cukup baik tentunya bukan hal mudah untuk Natus Vincere atau Na'Vi. Tim asal Ukraina ini kini resmi memasukan RAMZES kedalam...",
        key: "2021/04/05/ramzes-resmi-bergabung-dengan-navi",
      },
      {
        id: 4,
        title: "APAC Predator League 2020/21 Grand Final Dimulai! Indonesia Siap?",
        thumb: "https://thelazy.media/wp-content/uploads/2021/04/077696400_1548605102-predator3-218x150.jpg",
        author: "Joshua Hadinata",
        tag: "esports",
        time: "April 3, 2021",
        desc: "Asia-Pasific Predator League 2020/21 Grand Final atau APAC Predator League 2020/21 Grand Final kini siap kembali diadakan pada tanggal 6 April - 11 April 2021 untuk dua game yaitu Dota 2...",
        key: "2021/04/03/apac-predator-league-2020-21-siap-dimulai",
      },
      {
        id: 5,
        title: "Menangkan PMPL Ladies ID Season 3, Aerowolf Zoo Amankan Tiket SEA",
        thumb: "https://thelazy.media/wp-content/uploads/2021/04/166838927_1794586844036062_311619264822669110_n-218x150.jpg",
        author: "Joshua Hadinata",
        tag: "esports",
        time: "April 2, 2021",
        desc: "Industri esports saat ini sudah tak lagi berkesan eksklusif untuk para pria. Hal ini kembali dibuktikan dengan hadirnya sebuah turnamen papan atas untuk PUBG Mobile yaitu PUBG Mobile Pro League (PMPL)...",
        key: "2021/04/02/aerowolf-zoo-juara-pmpl-ladies-id-s3",
      },
    ],
  };
  render() {
    const { games } = this.state;
    return (
      <Main>
        <h1>Home Page</h1>
        <div className="container">
          {games.map((item) => (
            <div key={item.id} className="card">
              <Link to={`/detail/${item.id}`}>
                <img src={item.thumb} alt="Avatar" />
              </Link>
              <div className="card-body">
                <h4>
                  <b>{item.title}</b>
                </h4>
                <p>{item.desc}</p>
              </div>
            </div>
          ))}
        </div>
      </Main>
    );
  }
}

export default Home;
