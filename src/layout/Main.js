import React from "react";
import Navbar from "../components/Navbar";

const Main = (props) => {
  return (
    <React.Fragment>
      <Navbar />
      {props.children}
    </React.Fragment>
  );
};

export default Main;
